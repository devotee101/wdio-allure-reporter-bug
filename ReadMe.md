# Setup Package dependencies
```
npm i -g allure-commandline
npm i
```

# Tests
## A test with two assertions, both of which pass - OK
```
rm -rf ./allure-results/*
./node_modules/.bin/wdio wdio.conf.js --spec ./test/features/PassPass.feature
allure generate ./allure-results/ -o ./allure-results/html
```
## A test with an assertion that passes followed by one which fails - OK
```
rm -rf ./allure-results/*
./node_modules/.bin/wdio wdio.conf.js --spec ./test/features/PassFail.feature
allure generate ./allure-results/ -o ./allure-results/html
```

## A test with an assertion that fails followed by one which succeeds - ERROR!
```
rm -rf ./allure-results/*
./node_modules/.bin/wdio wdio.conf.js --spec ./test/features/FailPass.feature
allure generate ./allure-results/ -o ./allure-results/html
```