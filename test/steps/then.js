import checkTitle from '../support/check/checkTitle';
import isVisible from '../support/check/isVisible';

module.exports = function then() {
    this.Then(
        /^I expect that the title is( not)* "([^"]*)?"$/,
        checkTitle
    );

    this.Then(
        /^I expect that element "([^"]*)?" is( not)* visible$/,
        isVisible
    );
}