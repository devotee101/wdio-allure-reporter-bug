Feature: Google
  Background:
    Given I open the site "/"

  Scenario: Fail then Pass
    Then  I expect that element "#tsf > div.tsf-p > div.jsb > center > input[type='submit']:nth-child(2)" is not visible
    And   I expect that element "#tsf > div.tsf-p > div.jsb > center > input[type='submit']:nth-child(1)" is visible